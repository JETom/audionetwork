﻿from threading import Thread, RLock
from AudioGateway import AudioGateway
from SignalModulator import SignalModulator
import numpy as np

class AudioNetwork(Thread):
    signal_modulator = None
    audio_gateway = None
    stay_alive = True
    queue_read = []
    queue_write = []
    previous_frame = []

    def __init__(self, sample_rate=44800, low_freq=18500, high_freq=20500, segment_order=2, segment_length=0.02, packet_length=20):
        Thread.__init__(self)
        self.lock = RLock()
        self.packet_length = packet_length
        self.broadcast_length = 4 + packet_length * 8 / segment_order
        self.signal_modulator = SignalModulator(sample_rate, low_freq, high_freq, segment_order, segment_length)
        self.audio_gateway = AudioGateway(sample_rate, self.broadcast_length * segment_length, 1000)
        self.prefix = self.signal_modulator.getPrefixSegments()

    def getIndex(self, array, subarray):
        try:
            id = np.array(array).tostring().index(np.array(subarray).tostring())//np.array(array).itemsize
        except(ValueError):
            id = -1
        return id

    def filterFrame(self, frame):
        msg = None
        id = self.getIndex(self.previous_frame, self.prefix)
        if (id >= 0): #self.prefix is in previous frame
            merge_frame = self.previous_frame[id:] + frame
            if (len(merge_frame) >= self.broadcast_length): #the frames contain a message
                msg = merge_frame[len(self.prefix):self.broadcast_length]
                self.previous_frame = merge_frame[self.broadcast_length:]
            else: #corrupted message, remove everything until the self.prefix
                self.previous_frame = merge_frame[len(self.prefix):]
        else: #self.prefix is not in previous frame
            merge_frame = self.previous_frame[len(self.previous_frame) - len(self.prefix):] + frame
            id = self.getIndex(merge_frame, self.prefix)
            if (id >= 0): #self.prefix is splitted between previous and new frame
                self.previous_frame = merge_frame #on next recursion, the self.prefix will be in the previous frame
            else:
                self.previous_frame = frame #nothing to do in the current recursion
        return msg

    def run(self):
        self.audio_gateway.start()
        while (self.stay_alive == True):
            with self.lock:
                while (len(self.queue_write) > 0):
                    filtered_frame = self.queue_write.pop(0)
                    samples = self.signal_modulator.modulate(filtered_frame, self.packet_length)
                    self.audio_gateway.sendBroadcast(samples)
            samples = self.audio_gateway.receiveBroadcast()
            if len(samples) > 0:
                frame = self.signal_modulator.demodulate(samples)
                filtered_frame = self.filterFrame(frame)
                if filtered_frame != None:
                    string = self.signal_modulator.strFromSegments(filtered_frame)[0]
                    with self.lock:
                        self.queue_read.append(string)
        self.audio_gateway.close()
        self.audio_gateway.join() # wait until AudioGateway closes

    def read(self):
        string = ''
        with self.lock:
            if (len(self.queue_read) > 0):
                string = self.queue_read.pop(0)
        return string

    def write(self, string):
        queue = []
        for i in range(0, len(string), self.packet_length):
            queue.append(string[i:i+self.packet_length])
        with self.lock:
            for substring in queue:
                self.queue_write.append(substring)
        return

    def close(self):
        self.stay_alive = False


if __name__=="__main__":
    from time import time, sleep
    print "#### Start of __main__ ####"
    audio_network = AudioNetwork(sample_rate=44800,low_freq=18500, high_freq=20500)
    audio_network.start()
    print "broadcast_duration (with prefix)", audio_network.broadcast_length
    string = "Hello world, this is a message from another galaxy"
    #string = 'c'
    print "will write this :", string, "length:", len(string)
    t0 = time()
    #audio_network.write(string)
    t1 = time()
    print "write delay:", (t1 - t0) * 1000, "ms"
    audio_network.join(20)
    print "stopping AudioNetwork"
    audio_network.close()
    print "####  End of __main__  ####"

    #string = 'ABCD' * 5
    #content = audio_network.signal_modulator.modulate(string, 20)
    #exportable = np.array(content)
    #exportable = ((exportable - min(exportable)) / (max(exportable) - min(exportable)) * 255).round(0).astype(int)
    #print "buffer convertion"
    #buffer = "".join(map(chr, exportable))
    #print "export to WAV"
    #fio.exportWav(buffer, 44800, "sample.wav", 1)