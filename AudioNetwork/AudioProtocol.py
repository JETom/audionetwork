﻿from AudioNetwork import AudioNetwork

class AudioProtocol(object):
    """description of class"""
    audio_network = None
    buffer_content = ''
    buffer_length = 0

    def __init__(self, packet_limit=127,
                 sample_rate=44800, low_freq=18500, high_freq=20500, segment_order=2, segment_length=0.02, packet_length=20):
        self.audio_network = AudioNetwork(sample_rate, low_freq, high_freq, segment_order, segment_length, packet_length)
        self.audio_network.start()
        self.packet_length = packet_length
        self.packet_limit = packet_limit

    def read(self):
        string = self.audio_network.read()
        if len(string) == 0:
            return ''
        checksum = ord(string[0])
        remaining = ord(string[1])
        content = string[2:]
        if checksum == sum(map(ord, list(content))) % 127:
            if self.buffer_content == '':
                self.buffer_length = remaining
            self.buffer_content += content[:remaining]
            if len(self.buffer_content) == self.buffer_length:
                result = self.buffer_content
                self.buffer_content = ''
                self.buffer_length = 0
                return result
            else:
                return ''
        else:
            return None
            
    def write(self, string):
        string = string[:self.packet_limit]
        string_length = len(string)
        for i in range(0, len(string), self.packet_length - 2):
            substring = string[i:i+self.packet_length-2]
            checksum = sum(map(ord, list(substring))) % 127
            remaining = string_length - i
            substring = chr(checksum) + chr(remaining) + substring
            self.audio_network.write(substring)

    def join(self, timeout=None):
        if timeout == None:
            self.audio_network.join()
        else:
            self.audio_network.join(timeout)
    
    def close(self):
        self.audio_network.close()

if __name__=="__main__":
    from time import sleep
    print "start of __main__"

    #initiate the communicator (default parameters)
    audio_protocol = AudioProtocol()

    #listen for message
    print "listen..."
    for i in range(10):
        string = audio_protocol.read()
        if (string != None and len(string) > 0):
            print "packet received :", string
        sleep(1)
    print "listen finished"
    print audio_protocol.buffer_content
    #send a message
    string = "hello world!"
    print "send message:", string
    audio_protocol.write(string)
    sleep(1)

    #close correctly the communication
    audio_protocol.close()
    audio_protocol.join()

    print "end of __main__"