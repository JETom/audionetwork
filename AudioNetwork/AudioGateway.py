﻿from threading import Thread, RLock
import numpy as np
import pyaudio

class AudioGateway(Thread):
    """description of class"""
    stay_alive = True
    is_broadcasting = False
    queue_output = []
    queue_input = []

    def __init__(self, sample_rate, frame_rate, frame_limit=1000, lock=None):
        Thread.__init__(self)
        if lock == None:
            self.lock = RLock()
        else:
            self.lock = lock
        self.pa = pyaudio.PyAudio()
        self.stream = self.pa.open(format=pyaudio.paFloat32,
                                   channels=1,
                                   rate=sample_rate,
                                   output=True, input=True)
        self.stream.stop_stream()
        self.sample_rate = sample_rate
        self.frame_rate = frame_rate
        self.frame_limit = frame_limit

    def getLock(self):
        return self.lock

    def sendBroadcast(self, data):
        with self.lock:
            if len(self.queue_output) < self.queue_input:
                self.queue_output.append(data)
                self.is_broadcasting = True

    def receiveBroadcast(self):
        with self.lock:
            if len(self.queue_input) > 0:
                input = self.queue_input.pop(0)
            else:
                input = []
        return input

    def run(self):
        self.stream.start_stream()
        while (self.stay_alive):
            if self.is_broadcasting == True:
                self.stream.stop_stream()
                self.stream.start_stream() # stream reset
                while len(self.queue_output) > 0:
                    with self.lock:
                        samples = self.queue_output.pop(0)
                    buf = np.array(samples, dtype=np.float32).tobytes()
                    self.stream.write(buf)
                self.stream.stop_stream()
                with self.lock:
                    if len(self.queue_output) == 0:
                        self.is_broadcasting = False
                self.stream.start_stream() # stream reset
            elif len(self.queue_input) < self.frame_limit:
                buf = self.stream.read(int(self.frame_rate * self.sample_rate))
                samples = np.fromstring(np.array(buf), dtype=np.float32)
                with self.lock:
                    self.queue_input.append(samples)
        self.stream.close()
        self.pa.terminate()

    def close(self):
        self.stay_alive = False