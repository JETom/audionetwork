﻿from scipy.signal import butter, lfilter
from bitarray import bitarray
from math import sin, pi
import numpy as np
from collections import Counter

class SignalModulator(object):
    """description of class"""
    
    def __init__(self, sample_rate, low_freq, high_freq, seg_order=1, seg_duration=0.01):
        self.sample_rate = sample_rate
        self.nyquist_freq = 0.5 * sample_rate
        self.band = (low_freq, high_freq)
        self.seg_order = seg_order
        self.seg_quantity = 2 ** seg_order
        self.seg_length = int((high_freq - low_freq) / self.seg_quantity)
        self.seg_ranges = []
        for freq in range(low_freq, high_freq, self.seg_length):
            self.seg_ranges.append((freq, freq + self.seg_length - 1))
        self.setSegmentDuration(seg_duration)
        self.setPrefix()
        self.segments_in_byte = 8 / seg_order
        self.base_range = np.zeros(self.segments_in_byte, dtype=int)
        for i in range (self.segments_in_byte):
            self.base_range[self.segments_in_byte - 1 - i] = self.seg_quantity ** i
    def setPrefix(self):
        self.prefix = (self.freq_samples[0] + self.freq_samples[self.seg_quantity-1]) * 2

    def getPrefixSegments(self):
        return [0, self.seg_quantity-1] * 2

    def setSegmentDuration(self, duration):
        self.seg_duration = duration
        #Can be highly optimized by modulating a single sample of the Nyquist frequency
        self.freq_samples = []
        for i in range(self.seg_quantity):
            freq = self.seg_ranges[i][0] + self.seg_length / 2
            print duration * self.sample_rate ,freq
            samples_usable = int(freq*duration) * int(self.sample_rate/freq)
            samples_unusable = int(duration * self.sample_rate) - samples_usable
            temp_sample_range = range(samples_usable)
            self.freq_samples.append(map(lambda x: sin(2 * pi * freq * x / self.sample_rate), temp_sample_range)+[0.0]*samples_unusable)
            #temp_sample_range = range(int(duration * self.sample_rate))
            #self.freq_samples.append(map(lambda x: sin(2 * pi * freq * x / self.sample_rate), temp_sample_range))

    def getClosestSegment(self, freq):
        for segment in range(self.seg_quantity):
            if (freq >= self.seg_ranges[segment][0]) and (freq <= self.seg_ranges[segment][1]):
                return segment
        return -1

    def bandpassFilter(self, data, lowcut, highcut, order=6):
        b, a = butter(order, [lowcut / self.nyquist_freq, highcut / self.nyquist_freq], btype='band')
        return lfilter(b, a, data)

    def getFrequencyPeaks(self, data, thresold_coefficient=0.5):
        spectrum = np.fft.fft(data)
        threshold = thresold_coefficient * max(abs(spectrum))
        mask = abs(spectrum) > threshold
        freq = np.fft.fftfreq(len(spectrum))
        return freq[mask] * self.sample_rate

    def modulate(self, message, length=-1):
        ba = bitarray()
        if length == -1:
            ba.fromstring(message)
        else:
            ba.fromstring(message[:length])
        encoded_content = []
        for i in range(0, ba.length(), self.seg_order):
            seg = 0
            for bit in ba[i:i + self.seg_order]:
                seg = (seg << 1) | bit
            encoded_content.append(seg)
        encoded_content += [0] * (length * self.seg_quantity - len(encoded_content))
        samples = list(self.prefix)
        for i in encoded_content:
            samples += self.freq_samples[i]
        # the following bandpass filter might overflow, thus the ugly saturation after
        samples = self.bandpassFilter(samples, self.band[0], self.band[1], order=6)
        max_overflow = max(abs(samples))
        if max_overflow > 1.0:
            samples = np.array(samples) / max_overflow
        return samples

    def sortComplexSamples(self, samples):
        count_segments = Counter(map(lambda x: self.getClosestSegment(abs(x)), self.getFrequencyPeaks(samples)))
        if len(count_segments) == 1:
            return len(samples), count_segments.most_common()[0][0]
        if len(count_segments) == 2:
            return self.sortComplexSamples(samples[:len(samples)/2])
        return len(samples), -1

    def demodulate(self, samples):
        '''make sure the sample length is a multiple of seg_duration
        returns: str-demodulated content, int- remaining samples'''
        samples_in_segment = int(self.seg_duration * self.sample_rate)
        samples = self.bandpassFilter(samples, self.band[0], self.band[1], order=5)
        data = []
        position = 0
        while (len(samples) - position >= samples_in_segment):
            offset, segment = self.sortComplexSamples(samples[position:position+samples_in_segment])
            position += offset
            if segment != -1:
                data.append(segment)
        return data

    def strFromSegments(self, segments):
        string = ''
        for i in range(0, len(segments), self.segments_in_byte):
            #string += chr(int(sum(self.base_range * segments[i:i+self.segments_in_byte])))
            value = 0
            for j in segments[i:i+self.segments_in_byte]:
                value = value * 2 ** self.seg_order + j
            string += chr(value)
        remaining_segments = segments[len(string) * self.segments_in_byte:]
        return string, remaining_segments