from AudioNetwork import AudioNetwork
from time import sleep

audio_network = AudioNetwork(sample_rate=44800,
                             low_freq=18500, high_freq=20500,
                             segment_length=0.02, packet_length=20)
audio_network.start()

#listen for message
for i in range(10):
    string = audio_network.read()
    if (len(string) > 0):
        print "packet received :", string
    sleep(1)

# send a message
string = 'hello world'
print "sending packet :", string
audio_network.write(string)
sleep(1)

# close AudioNetwork()
audio_network.close()
audio_network.join()